﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace Bilietai.Controllers
{
    public class TicketController : Controller
    {
        // GET: Ticket
        public ActionResult Ticket()
        {
            return View();
        }

        [HttpPost]
        public ActionResult generateTicket()
        {
            using (MemoryStream ms = new MemoryStream())
            {
                
                string checkedValues = Request.Form["checkedValues"];
                DateTime fileCreationDatetime = DateTime.Now;
                string data = fileCreationDatetime.ToString(@"yyyy-MM-dd") + " " + fileCreationDatetime.ToString(@"HH:mm:ss");
                Rectangle envelope = new Rectangle(600, 200);
                Document doc = new Document(envelope, 20f, 10f, 30f, 20f);
                Font verdana = FontFactory.GetFont("Verdana", 14, Font.BOLD);
                PdfWriter writer = PdfWriter.GetInstance(doc, ms);
                doc.Open();
                Image logo = Image.GetInstance(Server.MapPath("~/images/logo.jpg"));
                logo.SetAbsolutePosition(480, 120);
                logo.ScalePercent(10f);
                doc.Add(logo);
                doc.Add(new Paragraph(data, verdana));
                doc.Add(new Paragraph("Movie: ", verdana));
                doc.Add(new Paragraph("Seat: " + checkedValues, verdana));
                doc.Add(new Paragraph("Price: ", verdana));
                doc.Close();
                byte[] bytes = ms.ToArray();
                ms.Close();

                return File(bytes, "application/pdf", "Ticket.pdf");
            }
        }
    }
}